﻿using System;

namespace BridgeSample.DataProvider
{
    /// <summary>
    /// Class StringDataProvider which allows to add simple string inputs
    /// </summary>
    public class StringDataProvider : ATextDataProvider
    {

        private readonly string text;

        /// <summary>
        /// Initializes a new instance of the <see cref="StringDataProvider"/> class.
        /// </summary>
        /// <param name="text">The text.</param>
        public StringDataProvider(int bridgeLength, int maxSectioLength, string text):base(bridgeLength, maxSectioLength)
        {
            this.text = text;
        }

        /// <summary>
        /// Gets the text lines.
        /// </summary>
        /// <value>The text lines.</value>

        protected override string line
        {
            get { return text; }
        }
    }

}

