﻿using System;
using System.Linq;
using BridgeSample.Exceptions;

namespace BridgeSample.DataProvider
{
    public class DataFactory
    {

        public static ADataProvider GetDataProvider(string[] args)
        {

            if (args.Length < 2)
                throw new ReadingException("Not anougth argumets");

            var a1 = ParcerHelper.TryToParcePositive(args[0]);
            var a2 = ParcerHelper.TryToParcePositive(args[1]);

            if (args.Length == 2)
            {
                return new RandomDataProvider(a1, a2);
            }

            if (args.Length == 3)
            {
                try
                {
                    ParcerHelper.TryToParcePositive(args[2]);
                }
                catch (NotNumberException e)
                {
                    return new FileDataProvider(a1, a2, args[2]);
                }
                catch (NegativeNumberException e)
                {
                    return new FileDataProvider(a1, a2, args[2]);
                }
            }

            return new StringDataProvider(a1, a2, String.Join( " ", args.Skip(2).ToArray()));
        }

    }
}

