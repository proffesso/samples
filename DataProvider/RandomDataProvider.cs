﻿using System;
using System.Collections.Generic;

namespace BridgeSample.DataProvider
{
    /// <summary>
    /// Class RandomDataProvider which generates the ramdome schedule untill the bridge will be build.
    /// </summary>
    public class RandomDataProvider:ADataProvider
    {
        static Random randomizer = new Random();

        public RandomDataProvider(int bridgeLength, int maxSectioLength):base(bridgeLength, maxSectioLength)
        {
            MaxSectionLength = maxSectioLength;
            BridgeLength = bridgeLength;
        }

        public override IEnumerable<int> ReadNextValue()
        {
            while (true)
            {
                yield return randomizer.Next(1, BridgeLength-1);
            }
        }
    }
}

