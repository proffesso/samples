﻿using System;

namespace BridgeSample.Exceptions
{
    /// <summary>
    /// Class ReadingException
    /// </summary>
    public class ReadingException : ApplicationException
    {
        private string wrongValue;

        public ReadingException(string wrongValue)
        {
            this.wrongValue = wrongValue;
        }

        protected virtual string Prefix
        {
            get { return "Wrong value"; }
        }

        public override string Message
        {
            get { return string.Format("{0} '{1}'", Prefix, wrongValue); }
        }
    }
}
