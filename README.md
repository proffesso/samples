# Building of the Bridge #

## Background ##

Consider the situation when two independent company trying to build the Bridge BGIDGE_LENGTH long.
Company A is responsible for providing the pillars. Each day it build somewhere a pillar.
Company B can immediately put the section if distance between pillars less then MAX_SECTION_LENGTH.

For example, if BGIDGE_LENGTH is 7 and MAX_SECTION_LENGTH is 3 and the schedule of work company A looks like [1, 3, 1, 4, 2, 5] (let's assume repeating numbers means repair works :) ), then on the 4th day will be possible to finish the bridge.

**Task**

Write a command-line program that reads BGIDGE_LENGTH and MAX_SECTION_LENGTH like arguments and the schedule company A from a text-format:

> BridgeSample 7 3 schedule.txt

> Bridge will be finished in 4 days

An average PC should be able to produce the answer (348577) for a example file (schedule.txt) within 0.5 seconds. Be aware that your code will be reviewed by a human and so your source files should be clear, easy to follow and maintainable, as well as your solution generating correct answers within the time bound.

We will be looking for the g to handle edge and error cases – such as being able to tell the end user whether and exactly where in the input there is an error (eg. not enough arguments or invalid values).